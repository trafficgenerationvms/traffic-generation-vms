#!/bin/bash
#
# Copyright (c) 2018 Conestoga College,
# Name: 
# Date: 
#
# This script will guide you through the process to set DHCP for the IP address
#
# This script will bootstrap these OSes:
#   - CentOS 7
#   - RHEL 7

# Save the name of the network connection as the variable connect_name

    connect_name="$(nmcli con sh | grep -F -f <(ip a | grep -o 'ens.*' | cut -d: -f1 | uniq | awk 'NR == 2') | awk '{print $1}')"

# The warning message about the script

    echo -e "-- \e[31mWARNING! \e[0m--"
    echo "This script will set DHCP for the network adapter $connect_name"
    echo "To terminate the script press Ctrl-C"
    echo ""
    printf "Press [Enter] to continue."
    read -r

# -----------------------------------------------------------------------------------------------------------------------------------
# Ask an IP address for another DNS (forwarder)
    
    echo ""
    echo -e "The default name for the connection is \e[35m$connect_name\e[0m"
    echo ""
    echo "Do you want to change the connection name for the script?"    
    read -p 'Please, enter y or n: '    response

# Check the user answer

    while true ; do
        if [[ "$response" == [Yy]* ]] || [[ "$response" == [Nn]* ]]; then
            break
        else
            echo ""
            echo "Sorry. You have entered the wrong answer."            
            read -p 'Please, enter y or n: '    response
        fi
    done

# Ask the user to enter the connection name alias

    if [[ "$response" == [Yy]* ]]; then

# Print the name of the devices

        nmcli con sh | awk '//{printf "\033[32m%-10s %s\n\033[0m",$1,$4}'

# Ask to choce the connection name and loop until the right choice     

        echo ""
        echo "Chose the connection name for the script"         
        read -p 'The connection name: ' connect_name


        while [[ -z "$connect_name" || "$connect_name" != "$(nmcli con sh | awk 'FNR>1 {print $1}' | grep -w "$connect_name")" ]] ; do
            echo ""
            echo -e "\e[32mSorry. You have entered the wrong connection name.\e[0m"
            echo ""
            echo "The connection name should be one from the list:"
            nmcli con sh | awk '//{printf "\033[32m%-10s %s\n\033[0m",$1,$4}'
            echo ""
            echo "Please, enter the connection name or press Ctrl-C to exit" 
            read -p 'The connection name: ' connect_name
        done

        echo ""
        echo -e "The connection name is \e[35m$connect_name\e[0m"
   fi
# -----------------------------------------------------------------------------------------------------------------------------------

# IP adress for the NIC   
 
    ip="$(nmcli con sh id $connect_name | grep IP4.ADDRESS | awk '{print $2}')"

# Define the device name according to the alias of the second network connection
   
   device="$(nmcli con sh id $connect_name | grep GENERAL.DEVICES | awk '{print $2}')"
    
 # Set DHCP for the second network connection

    nmcli con mod $connect_name ipv4.method auto

# Delete the IP adress and DNS record from the second network adapter

    nmcli con mod $connect_name -ipv4.address $ip

# Calculate the network address according to the IP from the user

    network="$(ipcalc -n $(nmcli con sh id $connect_name | grep IP4.ADDRESS | awk '{print $2}') | cut -d= -f2)"
    
# Take the last octet the network address

    last_net_oct="$(echo "$network" | cut -d'.' -f 4)"

# Increment the last octet by 1

    last_net_octet_inc="$(($last_net_oct +1))"

# The first IP from the network. Take first three octets from the network ipcalc and concatenate with "." and the last octet

    first_IP="$(echo "$network" | cut -d'.' -f 1-3 )$(echo ".")$last_net_octet_inc"

# Delete the static route the second network adapter

    nmcli con mod $connect_name -ipv4.routes "172.16.0.0/12 $first_IP"

# Set auto routes and default routes for the second connection as default

    nmcli con mod $connect_name ipv4.ignore-auto-routes no
    nmcli con mod $connect_name ipv4.never-default no

# Bring the connection up 

    nmcli connection up $connect_name
    
# Restart network manager service
    
    echo "Restart the network service"
    if ! systemctl restart network # if service is failled
    then
        echo ""
        echo -e "\e[31mThe network service restart is failled\e[0m"
        exit 1
    fi

# The IP reset complete
    
    echo ""
    echo "Complete!"
    
exit 0