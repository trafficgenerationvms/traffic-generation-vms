#
# Copyright (c) 2018 Conestoga College,
# Name: Filip Balos
# Date: August 12, 2018
#
# This script deploys VMs into a vSphere cloud from a preexisting template stored on a shared datastore.
# The script checks for the availability of necessary infrastructure objects including a vSphere cluster,
# distributed vSwitch and portgroups, shared datastore and VM folder. User prompts allow the user to select
# the appropriate objects.
#
# A CSV spreadsheet is required specifying the VM name, Equipment adapter distributed portgroup and Production
# adapter IP for each VM to be deployed. A sample CSV file generated from the "Traffic_Injection_VM_and_Network_Configuration"
# project document is provided with the script.
#


# Stop in case of errors in subcommands
$ErrorActionPreference = "Stop"

#Equipment network IP for VMs
$equipIP = "172.31.255.254/24"

#Production network mask
$prodMask = "/22"

# Get IP of vCenter server
$vCenterServer = read-host "`nEnter the IP or FQDN of the vCenter Server to connect to"

# Deploy VM
function Deploy-VM
{
  param($VMName, $SCluster, $deployTemplate, $SDatastore, $SFolder, $prodVDPortgroup, $VMEquipDPortgroup, $prodIP, $equipIP, $guestCredential)
  # Deploy new VM
  Write-Host -ForegroundColor Green "`nCreating VM $VMName"
  New-VM -Name $VMName -ResourcePool $SCluster -Template $deployTemplate -Datastore $SDatastore -DiskStorageFormat Thin -Location $SFolder -Confirm:$False
  Start-Sleep -s 3
  # Set Portgroups
  Set-Portgroups $VMName  $prodVDPortgroup  $VMEquipDPortgroup
  # Start VM
  Start-VM-With-Tools $VMName
  #Set VM hostname and assign IPs
  Set-VM-Settings $VMName  $guestCredential  $prodIP  $equipIP
  Start-Sleep -s 3
}

# Assign Portgroups to the network adapters on the VM
function Set-Portgroups
{
  param($VMName, $prodVDPortgroup, $VMEquipDPortgroup)
  # Production Network Adapter
  Write-Host -ForegroundColor Green "Assigning Network Adapter 1 to the Production portgroup"
  Get-VM $VMName |Get-NetworkAdapter |Where {$_.Name -eq "Network adapter 1"} |Set-NetworkAdapter -Portgroup $prodVDPortgroup -Confirm:$False
  
  # Equipment Network Adapter
  Write-Host -ForegroundColor Green "Assigning Network Adapter 2 to the Equipment portgroup"
  Get-VM $VMName |Get-NetworkAdapter |Where {$_.Name -eq "Network adapter 2"} |Set-NetworkAdapter -Portgroup $VMEquipDPortgroup -Confirm:$False
}
  
# Start the VM
function Start-VM-With-Tools
{
  param($VMName)
  $vm = Get-VM $VMName
  if ($vm.PowerState -ne "PoweredOn")
  {
    Write-Host -ForegroundColor Green "VM is not powered on, starting $VMName"
    Start-VM -VM $VMName -Confirm:$False
    Write-Host -ForegroundColor Green "Waiting 60 seconds for VM to boot, if the Invoke-VMScript commandlet fails and VMWare Tools is running this delay may need to be extended"
    Start-Sleep -s 60
  }
  Start-Sleep -s 3
  
  Write-Host -ForegroundColor Green "Checking for VMWare Tools..."
  Try
  {
      Wait-Tools -VM $vm -TimeoutSeconds 180
      Write-Host -ForegroundColor Green "VMWare Tools on $VMName is running"
  }
  Catch
  {
      Write-Error "Error: VMWare Tools is not installed or not running"
  }
}
  
 #Set VM hostname and assign IPs
function Set-VM-Settings
{
  param($VMName, $guestCredential, $prodIP, $equipIP)
  Start-Sleep -s 3
  Write-Host -ForegroundColor Green "Setting VM hostname to $VMName"
  Get-VM $VMName | Invoke-VMScript -ScriptType bash -ScriptText "nmcli general hostname `"$VMName`"" -GuestCredential $guestCredential

  $guestConnDevs = Get-VM $VMName | Invoke-VMScript -ScriptType bash -ScriptText "nmcli con reload && nmcli -g GENERAL.HWADDR,GENERAL.CONNECTION -e no device show" -GuestCredential $guestCredential
  $guestConnDevs = $guestConnDevs -split('\n')

  #Assign VM IPs
  Start-Sleep -s 3
  $prodMAC = Get-VM $VMname | Get-NetworkAdapter -Name "Network adapter 1"
  $prodMAC = $prodMAC.MacAddress
  $equipMAC = Get-VM $VMname | Get-NetworkAdapter -Name "Network adapter 2"
  $equipMAC = $equipMAC.MacAddress

  for ($i=0; $i -lt $guestConnDevs.length; $i++)
  {
    if ($guestConnDevs[$i] -like $prodMAC)
    {
      $guestConn = $guestConnDevs[$i+1]
      Write-Host -ForegroundColor Green "Setting interface"$guestConnDevs[$i+1]"to Production with IP $prodIP"
      Get-VM $VMName | Invoke-VMScript -ScriptType bash -ScriptText "nmcli connection modify `"$guestConn`" connection.id Production && nmcli con mod Production ipv4.addresses $prodIP && nmcli con mod Production ipv4.method manual && nmcli connection up Production" -GuestCredential $guestCredential
    }
    elseif ($guestConnDevs[$i] -like $equipMAC)
    {
      $guestConn = $guestConnDevs[$i+1]
      Write-Host -ForegroundColor Green "Setting interface"$guestConnDevs[$i+1]"to Equipment with IP $equipIP"
      Get-VM $VMName | Invoke-VMScript -ScriptType bash -ScriptText "nmcli connection modify `"$guestConn`" connection.id Equipment && nmcli con mod Equipment ipv4.addresses $equipIP && nmcli con mod Equipment ipv4.method manual && nmcli connection up Equipment" -GuestCredential $guestCredential
    }
  }
}

# connect to selected vCenter
connect-viserver $vCenterServer

# Get the template to deploy
$availTemplates = Get-Template -Server $vCenterServer
if (!($availTemplates))
{
  Write-Error "No templates available on vCenter server, please upload one."
}
Write-Host -ForegroundColor Green "`nAvailable templates on ${vCenterServer}: ${availTemplates}"

$i = 1
$availTemplates | %{Write-Host $i":" $_.Name; $i++}
$deployTemplate = Read-host "`nEnter the number for the template to be deployed"
$deployTemplate = $availTemplates[$deployTemplate -1].Name
Write-Host -ForegroundColor Green "`nTemplate to be deployed: $($deployTemplate)"
Start-Sleep -s 3

# Choose which Cluster to deploy to
$availClusters = Get-Cluster | Select Name | Sort-object Name
if (!($availClusters))
{
  Write-Error "No clusters available on this vCenter server."
}
Write-Host -ForegroundColor Green "`nChoose a Cluster to deploy VMs into: "

$i = 1
$availClusters | %{Write-Host $i":" $_.Name; $i++}
$deployCluster = Read-host "`nEnter the number for the cluster to deploy to"
$SCLUSTER = $availClusters[$deployCluster -1].Name
Write-Host -ForegroundColor Green "`nCluster to be deployed to: $($SCLUSTER)."
Start-Sleep -s 3

# Choose a Datastore to deploy to
$availDatastores = Get-Cluster -Name $SCluster | Get-Datastore | where{$_.ExtensionData.Summary.MultipleHostAccess}
if (!($availDatastores))
{
  Write-Error "No datastore clusters available on this vCenter cluster."
}
Write-Host -ForegroundColor Green "`nChoose a shared datastore on the selected cluster to deploy to:"

$i = 1
$availDatastores | %{Write-Host $i":" $_.Name; $i++}
$DSIndex = Read-Host "`nEnter a number for the datastore to deploy to" 
$SDatastore = $availDatastores[$DSIndex - 1].Name
Write-Host -ForegroundColor Green "VMs will be deployed to the $SDatastore datastore"
Start-Sleep -s 3

# Choose which Virtual Switch The Required Network will belong to
$availVDSwitches = Get-VDSwitch | Select Name | Sort-Object Name
if (!($availVDSwitches))
{
  Write-Error "No distributed vSwitches available on this vCenter cluster."
}
Write-Host -ForegroundColor Green "`nChoose a distributed vSwitch to be used:"

$i = 1
$availVDSwitches | %{Write-Host $i":" $_.Name; $i++}
$VDSwitchIndex = Read-Host "`nEnter a number for the distributed vswitch to deploy to" 
$SVDSwitch = $availVDSwitches[$DSIndex - 1].Name
Write-Host -ForegroundColor Green "The distributed vSwitch $SVDSwitch will be used"
Start-Sleep -s 3

# Check for existence of production distributed portgroup
$defProdVDPortgroup = "WTEIT3APG - Production"
$prodVDPortgroup = Read-Host "Enter the name of the Production Portgroup [$($defProdVDPortgroup)]"
if ($prodVDPortgroup -eq $null -or $prodVDPortgroup -eq "")
{
  $prodVDPortgroup = $defProdVDPortgroup
}
if (Get-VDPortgroup $prodVDPortgroup) 
{ 
  Write-Host -ForegroundColor Green "`nThe production portgroup `"$prodVDPortgroup`" will be assigned to the first network card on each VM"
} 
else 
{
  Write-Error "The production portgroup `"$prodVDPortgroup`" does not exist on this cluster"
}

# Folder Selection
$availFolders = Get-Folder | Select Name | Sort-Object Name
if (!($availFolders))
{
  Write-Error "No folders available on this vCenter cluster"
}
Write-Host -ForegroundColor Green "`nSelect which folder to store the VM..."

$i = 1
$availFolders | %{Write-Host $i":" $_.Name; $i++}
$FSIndex = Read-Host "Select a Folder. Enter a number ( 1 -" $availFolders.Count ")"
$SFOLDER = $availFolders[$FSIndex - 1].Name
Write-Host -ForegroundColor Green "`nVMs will be deployed to the folder: "$SFOLDER
Start-Sleep -s 3

# Let user choose CSV file
Write-Host -ForegroundColor Green "Please choose a CSV VM template file"
Add-Type -AssemblyName System.Windows.Forms
$FileBrowser = New-Object System.Windows.Forms.OpenFileDialog -Property @{
  Multiselect = $false # Multiple files can be chosen
  Filter = "CSV (*.csv)| *.csv" # Specified file types
}

[void]$FileBrowser.ShowDialog()

$file = $FileBrowser.FileName;

If($FileBrowser.FileNames -like "*\*")
{
  Write-Host -ForegroundColor Green "`nUsing $file as VM list"

}
else
{
  Write-Host -ForegroundColor Green "`nCancelled by user"
}

#Guest VM credentials
$guestCredential=Get-Credential -Message "Enter the credentials for connecting to guest VMs"

# Check whether to delete existing VMs
$VMExistsOption = Read-Host "`nWhat to do with existing VMs? (D)elete and re-deploy, (R)e-assign portgroups and IPs, (S)kip"

# Read Name, IP, VLAN ID and Equipment Portgroup from CSV file
foreach ($Row in (import-csv $file)) 
{
  $ProdIP = $Row.IP
  $ProdIP = "$prodIP$prodMask"
  $VMName = $Row.Name
  $VMVLAN = $Row.VLAN
  $VMEquipDPortgroup = $Row.DPortgroup

  # Check for existence of equipment distributed portgroup
  if (!(Get-VDPortgroup $VMEquipDPortgroup))
  { 
    Write-Error "The equipment portgroup `"$prodVDPortgroup`" does not exist on this cluster"
  }

  Switch -RegEx ($VMExistsOption)
  {
    ## Check for existing VM and destroy it
    '^D|^d'
    {
      Try
      {
        $vm = Get-VM $VMName
        if ($vm)
        {
          Write-Host -ForegroundColor Green "`nVM $VMName exists..."
          if ($vm.PowerState -eq "PoweredOn")
          {
            Write-Host -ForegroundColor Green "`nVM $VMName shutting down..."
            Stop-VM -VM $vm -Confirm:$False
            Start-Sleep -s 3
          }
            Write-Host -ForegroundColor Green "`nVM $VMName deleting..."
            Remove-VM -VM $vm -DeletePermanently -Confirm:$False
        }
        # Deploy new VM
        Deploy-VM $VMName  $SCluster  $deployTemplate  $SDatastore  $SFolder  $prodVDPortgroup  $VMEquipDPortgroup  $prodIP  $equipIP  $guestCredential
      }
      Catch
      {
        # Deploy new VM
        Deploy-VM $VMName  $SCluster  $deployTemplate  $SDatastore  $SFolder  $prodVDPortgroup  $VMEquipDPortgroup  $prodIP  $equipIP  $guestCredential
      }
    }
    '^R|^r'
    {
      Try
      {
        $vm = Get-VM $VMName
        if ($vm)
        {
          Write-Host -ForegroundColor Green "`nVM $VMName exists, reconfiguring..."
          # Set Portgroups
          Set-Portgroups $VMName  $prodVDPortgroup  $VMEquipDPortgroup
          # Start VM
          Start-VM-With-Tools $VMName
          #Set VM hostname and assign IPs
          Set-VM-Settings $VMName  $guestCredential  $prodIP  $equipIP
        }
      }
      Catch
      {
        # Deploy new VM
        Deploy-VM $VMName  $SCluster  $deployTemplate  $SDatastore  $SFolder  $prodVDPortgroup  $VMEquipDPortgroup  $prodIP  $equipIP  $guestCredential
      }
    }
    '^S|^s'
    {
      Try
      {
        $vm = Get-VM $VMName
        if ($vm)
        {
          Write-Host -ForegroundColor Green "`nVM $VMName exists, skipping..."
        }
      }
      Catch
      {
        # Deploy new VM
        Deploy-VM $VMName  $SCluster  $deployTemplate  $SDatastore  $SFolder  $prodVDPortgroup  $VMEquipDPortgroup  $prodIP  $equipIP  $guestCredential
      }
    }
  }
}
  

Write-Host -ForegroundColor Green "`nAll VMs deployed successfully"

# Disconnect from vCenter
Disconnect-viserver $vCenterServer -Confirm:$false
